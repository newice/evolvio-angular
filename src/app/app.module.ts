import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { CoreComponent } from './core/core.component';
import { TileService } from './map/services/tile.service';
import { SeededNoiseService } from './core/services/seeded-noise.service';
import { CreatureService } from './core/services/creature.service';
import { ConfigService } from './core/services/config.service';
import { DurationPipe } from './core/pipes/duration.pipe';


@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    CoreComponent,
    DurationPipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [SeededNoiseService, TileService, CreatureService, ConfigService],
  bootstrap: [AppComponent]
})
export class AppModule { }
