CanvasRenderingContext2D.prototype.fillCircle = function (x, y, r, s) {
  this.beginPath();
  this.arc(x, y, r, 0, 2 * Math.PI);
  this.closePath();

  this.fill();
  if (s) {
    this.stroke();
  }
};

