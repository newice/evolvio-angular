import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import './renderer.js';
import { TileService } from './services/tile.service';
import { Tile } from '../core/models/tile';
import { SeededNoiseService } from '../core/services/seeded-noise.service';
import { TileTypes } from './enums/tile-types.enum';
import { ConfigService } from '../core/services/config.service';
import { BaseCreature } from '../core/models/base-creature';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements AfterViewInit {
  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('viewport') viewport: ElementRef;
  @Input('creatures') creatures: BaseCreature[];
  @Input('map') map: Tile[][];
  ctx: CanvasRenderingContext2D;
  ctz: CanvasRenderingContext2D;
  outline = [];
  zoomLevel = 1 / 2;
  cropx = 0;
  cropy = 0;
  selectedCreature: BaseCreature;

  @Output() creatureSelected = new EventEmitter<BaseCreature>();

  constructor(private _config: ConfigService, private _noise: SeededNoiseService, private _tileService: TileService) {


    window.onmousewheel = (e) => {
      this.zoomLevel += e.wheelDelta / _config.zoomSpeed / (2400 / this.zoomLevel);

      if (this.zoomLevel < _config.minZoomLevel) {
        this.zoomLevel = _config.minZoomLevel;
      }

      if (this.zoomLevel > _config.maxZoomLevel) {
        this.zoomLevel = _config.maxZoomLevel;
      }

    };

  }

  ngAfterViewInit() {
    this.outline = this._tileService.generateOutline(this.map, this._config.tileSize);
    this.ctx = this.canvas.nativeElement.getContext('2d', { alpha: false });
    this.ctz = this.viewport.nativeElement.getContext('2d', { alpha: false });
    this.render();

    this.canvas.nativeElement.onmousedown = (e) => {
      const p = this.getCoords(e);

      this.setActiveCreature(p);
      this.updateFrame(p);
    };

    this.viewport.nativeElement.onmousedown = (e) => {
      const p = this.getViewportCoords(e);
      this.setActiveCreature(p);
    };

    this.canvas.nativeElement.onmousemove = (e) => {
      if (e.buttons) this.updateFrame(this.getCoords(e));
    };
  }

  setActiveCreature(p: { x: any, y: any }) {
    if (this.selectedCreature) {
      this.selectedCreature.selected = false;
      this.selectedCreature = null;
    }

    let active = this.creatures.filter(c => Math.abs(p.x - c.pos.x) < c.size && Math.abs(p.y - c.pos.y) < c.size);

    if (active && active[0]) {
      active[0].selected = true;
      this.selectedCreature = active[0];
    }

    this.creatureSelected.emit(this.selectedCreature);
  }

  updateFrame(p: { x, y }) {
    const viewport = this.viewport.nativeElement;
    this.cropx = Math.max(0, Math.min(this.canvas.nativeElement.width - viewport.width / this.zoomLevel, p.x - viewport.width / this.zoomLevel / 2));
    this.cropy = Math.max(0, Math.min(this.canvas.nativeElement.height - viewport.height / this.zoomLevel, p.y - viewport.height / this.zoomLevel / 2));
  }

  getCoords(e): { x, y } {
    const canvas = this.canvas.nativeElement;
    const f = canvas.width / canvas.clientWidth;
    const g = canvas.height / canvas.clientHeight;
    const x = (e.clientX - canvas.getBoundingClientRect().left) * f;
    const y = (e.clientY - canvas.getBoundingClientRect().top) * g;

    return { x: x, y: y };
  }

  getViewportCoords(e): { x, y } {
    const viewport = this.viewport.nativeElement;
    const f = viewport.width / viewport.clientWidth;
    const g = viewport.height / viewport.clientHeight;
    const x = this.cropx + (e.clientX - viewport.getBoundingClientRect().left) * f / this.zoomLevel;
    const y = this.cropy + (e.clientY - viewport.getBoundingClientRect().top) * g / this.zoomLevel;

    return { x: x, y: y };
  }

  render() {
    this.renderBackground();
    this.renderTiles();
    this.renderOutline();
    this.renderCreatures();
    this.renderViewPort();
    this.renderZoomFrame();
    requestAnimationFrame(() => { this.render(); });
  }
  renderZoomFrame() {
    if (this.selectedCreature) {
      this.updateFrame(this.selectedCreature.pos);
    }
    this.ctx.strokeStyle = '#000000';
    this.ctx.lineWidth = 15;
    const viewport = this.viewport.nativeElement;

    this.ctx.strokeRect(this.cropx, this.cropy, viewport.width / this.zoomLevel, viewport.height / this.zoomLevel);
  }

  renderViewPort() {
    const viewport = this.viewport.nativeElement;
    const display = this.canvas.nativeElement;
    this.ctz.clearRect(0, 0, viewport.width, viewport.height);
    this.ctz.drawImage(display, this.cropx, this.cropy, viewport.width / this.zoomLevel, viewport.height / this.zoomLevel, 0, 0, viewport.width, viewport.height);


  }

  renderBackground(): any {
    const display = this.canvas.nativeElement;
    this.ctx.clearRect(0, 0, display.width, display.height);
    this.ctx.fillStyle = '#1799B5';
    this.ctx.fillRect(0, 0, display.width, display.height);
  }

  renderTiles() {
    if (this.map) {
      this.map.forEach((row: Tile[], i) => {
        row.forEach((tile: Tile, j) => {
          if (tile.Type === TileTypes.Water) {
            return;
          }
          this.ctx.fillStyle = this._tileService.tileColor(tile);
          this.ctx.fillRect(i * this._config.tileSize, j * this._config.tileSize, this._config.tileSize, this._config.tileSize);
        });
      });
    }
  }

  renderOutline() {
    this.ctx.strokeStyle = '#ffffff';
    this.ctx.lineWidth = 5;
    this.outline.forEach((item) => {
      this.ctx.beginPath();
      this.ctx.moveTo(item[0], item[1]);
      this.ctx.lineTo(item[2], item[3]);
      this.ctx.stroke();
    });
  }

  renderCreatures() {
    this.ctx.lineWidth = 5;
    for (const creature of this.creatures) {
      this.ctx.strokeStyle = '#ffffff';
      if (creature.selected) {
        this.ctx.strokeStyle = '#ff0000';
      }

      this.ctx.fillStyle = creature.color;
      this.ctx.fillCircle(creature.pos.x, creature.pos.y, creature.size, true);
    }
  }



}
