import { Injectable } from '@angular/core';
import { rootRenderNodes } from '@angular/core/src/view/util';
import { Tile } from '../../core/models/tile';
import { TileTypes } from '../enums/tile-types.enum';
import { SeededNoiseService } from '../../core/services/seeded-noise.service';

@Injectable()
export class TileService {

  propability = 60;
  maxTileFood = 1000;
  constructor(private _noise: SeededNoiseService) { }

  public newTile(): Tile {
    const tile = new Tile();
    tile.Type = this._noise.noiseF(100) > this.propability ? TileTypes.Water : TileTypes.Land;
    tile.Food = tile.Type === TileTypes.Land ? this.maxTileFood / 1.2 : 0;
    return tile;
  }

  public generateMap(rows: number, cols: number): Tile[][] {
    const map = [];
    for (let i = 0; i < rows; i++) {
      const row = [];
      for (let j = 0; j < cols; j++) {
        row.push(this.newTile());
      }
      map.push(row);
    }

    return map;
  }

  generateOutline(map: Tile[][], tileSize: number) {
    const outline = [];
    map.forEach((row: Tile[], i) => {
      row.forEach((tile: Tile, j) => {
        if (tile.Type === TileTypes.Water) {
          return;
        }

        if (i < map.length - 1 && map[i + 1][j].Type === TileTypes.Water) {
          outline.push([
            (i + 1) * tileSize,
            j * tileSize,
            (i + 1) * tileSize,
            (j + 1) * tileSize
          ]);
        }

        if (i > 0 && map[i - 1][j].Type === TileTypes.Water) {
          outline.push([
            i * tileSize,
            j * tileSize,
            i * tileSize,
            (j + 1) * tileSize
          ]);
        }

        if (j < row.length - 1 && map[i][j + 1].Type === TileTypes.Water) {
          outline.push([
            i * tileSize,
            (j + 1) * tileSize,
            (i + 1) * tileSize,
            (j + 1) * tileSize
          ]);
        }

        if (j > 0 && map[i][j - 1].Type === TileTypes.Water) {
          outline.push([
            i * tileSize,
            j * tileSize,
            (i + 1) * tileSize,
            j * tileSize
          ]);
        }
      });
    });

    return outline;
  }

  public tileColor(tile: Tile) {
    return 'hsl(90,' + Math.round(tile.Food / this.maxTileFood * 100) + '%, 32%)';
  }
}
