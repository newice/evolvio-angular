import { Component } from '@angular/core';
import { CreatureService } from './core/services/creature.service';
import { BaseCreature } from './core/models/base-creature';
import { ConfigService } from './core/services/config.service';
import { MapComponent } from './map/map.component';
import { Tile } from './core/models/tile';
import { TileService } from './map/services/tile.service';
import { keys, controls } from './core/const/keys';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  creatures: BaseCreature[] = [];
  map: Tile[][];
  timescale = 1;
  tc = 0;
  popMax = 0;
  frameCount = 0;
  constructor(private _config: ConfigService, private _creatureService: CreatureService, private _tileService: TileService) {
    this.map = _tileService.generateMap(this._config.mapSize, this._config.mapSize);
    for (let i = 0; i < _config.minCreatures; i++) {
      this.creatures.push(_creatureService.newCreature());
    }
  }

  selectedCreature: BaseCreature;

  ngOnInit() {
    setInterval(() => { this.update(); }, 1000 / 60);

    window.onkeydown = (e) => {
      const key = e.keyCode;
      switch (key) {
        case keys[controls.fastForward]:
          this.timescale = 50;
          break;
        case keys[controls.play]:
          this.timescale = 1;
          break;
        case keys[controls.stop]:
          this.timescale = 0;
          break;
        case keys[controls.speedUp]:
          this.timescale += 10;
          break;
        default:
          this.timescale = 1;
      }
    };

    window.onkeyup = (e) => {
      const key = e.keyCode;
      switch (key) {
        case keys[controls.fastForward]:
          this.timescale = 1;
          break;
      }
    };
  }

  update() {
    if (this.timescale >= 1) {
      for (let ts = 0; ts < this.timescale; ts++) {
        this._creatureService.update(this.creatures, this.map);
        this.frameCount++;
      }
    } else {
      this.tc++;
      if (this.tc > 1 / this.timescale) {
        this._creatureService.update(this.creatures, this.map);
        this.frameCount++;
        this.tc = 0;
      }
    }
    if (this.creatures) {
      this.popMax = Math.max(this.popMax, this.creatures.length);
    }
  }
  setCreature(creature:BaseCreature) {
    this.selectedCreature = creature;
  }

}