import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {

  transform(value: number, args?: any): string {
    let result = '';
    value = Math.floor(value);
    let seconds = 60 * 60 * 24 * 365;
    if (value > seconds) {
      const years = Math.floor(value / seconds)
      result += `${years} Years `;
      value =value % seconds;
    }
    seconds = 60 * 60 * 24 ;
    if (value > seconds) {
      const years = Math.floor(value / seconds)
      result += `${years} Days `;
      value =value % seconds;
    }
    seconds = 60 * 60  ;
    if (value > seconds) {
      const years = Math.floor(value / seconds)
      result += `${years} Housrs `;
      value =value % seconds;
    }
    seconds = 60   ;
    if (value > seconds) {
      const years = Math.floor(value / seconds)
      result += `${years} Minutes `;
      value =value % seconds;
    }

    result += `${value} Seconds `;

    return result;
  }

}
