import { Network } from './network';

export class BaseCreature {
    public network: Network;
    public output: number[];
    public pos: {x:number, y:number};
    public size: number;
    public generation: number;
    public age: number;
    public reproduceTime: number;
    public maxSpeed: number;
    public color: string;
    public selected: boolean;

}
