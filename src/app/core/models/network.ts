export class Network {
    public layers: number[];
    public neurons: number[][];
    public axons: number[][][];
}
