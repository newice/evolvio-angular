import { TileTypes } from '../../map/enums/tile-types.enum';


export class Tile {
    public Type: TileTypes;
    public Food: number;
}
