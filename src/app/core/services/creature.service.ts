import { Injectable } from '@angular/core';
import { BaseCreature } from '../models/base-creature';
import { Network } from '../models/network';
import { SeededNoiseService } from './seeded-noise.service';
import { ConfigService } from './config.service';
import { Tile } from '../models/tile';
import { TileService } from '../../map/services/tile.service';
import { TileTypes } from '../../map/enums/tile-types.enum';

@Injectable()
export class CreatureService {
  constructor(private _noise: SeededNoiseService, private _config: ConfigService) { }
  newCreature(): BaseCreature {
    const creature = <BaseCreature>{
      output: [0, 0, 0],
      network: this.initNetwork(),
      pos: {
        x: this._noise.noiseF(this._config.fieldWidth),
        y: this._noise.noiseF(this._config.fieldHeight),
      },
      size: this._noise.noiseF((this._config.maxCreatureSize - this._config.minCreatureSize) / 2) + this._config.minCreatureSize,
      generation: 1,
      age: 0,
      reproduceTime: 0,
      color: this.newColor(),
      maxSpeed: this._config.maxCreatureSpeed,
      selected: false,
    };

    return creature;
  }

  newColor(): string {
    const h = Math.floor(this._noise.noiseF(360));
    const s = Math.floor(this._noise.noiseF(100));
    const l = Math.floor(this._noise.noiseF(100));

    return `hsl(${h}, ${s}%, ${l}%)`;
  }

  initNetwork(): Network {
    const network = <Network>{
      layers: this._config.layers,
      neurons: [],
      axons: []
    };

    for (let i = 0; i < this._config.layers.length; i++) {
      const layerNeurons = Array(this._config.layers[i]).fill(0);
      network.neurons.push(layerNeurons);
    }

    for (let i = 1; i < this._config.layers.length; i++) {
      const layerWeights = [];
      const neuronsInPreviousLayer = this._config.layers[i - 1];
      for (let j = 0; j < this._config.layers[i]; j++) {
        const neuronWeights = [];
        for (let k = 0; k < neuronsInPreviousLayer; k++) {
          neuronWeights.push(this._noise.noiseF(2) - 1);
        }

        layerWeights.push(neuronWeights);
      }

      network.axons.push(layerWeights);

    }
    return network;
  }

  copyNeuralNetwork(source: BaseCreature, dest: BaseCreature) {
    for (let i = 0, l = dest.network.axons.length; i < l; i++) {
      for (let j = 0, m = dest.network.axons[i].length; j < m; j++) {
        for (let k = 0, n = dest.network.axons[i][j].length; k < n; k++) {
          dest.network.axons[i][j][k] = source.network.axons[i][j][k];
        }
      }
    }
  }

  feedForward(creature: BaseCreature, inputs: number[]) {
    creature.network.neurons[0] = inputs; // Takes the inputs and applies them

    for (let i = 1; i < creature.network.layers.length; i++) { // i = layer
      for (let j = 0; j < creature.network.layers[i]; j++) { // j = layerNeuron
        let value = this._config.offset;
        for (let k = 0; k < creature.network.layers[i - 1]; k++) { // k = prevLayerNeuron
          value += creature.network.axons[i - 1][j][k] * creature.network.neurons[i - 1][k]; // Adds the neurons value * the weight
        }
        creature.network.neurons[i][j] = Math.tanh(value); // Sets the neuron across from the axon to the new value (1 - -1)
      }
    }

    return creature.network.neurons[creature.network.neurons.length - 1]; // returns the output, or the last line

  }

  getTile(creature: BaseCreature, map: Tile[][]): Tile {
    const x = Math.floor(creature.pos.x / this._config.tileSize);
    const y = Math.floor(creature.pos.y / this._config.tileSize);

    return map[x][y];
  }

  move(creature: BaseCreature, type: TileTypes) {
    creature.size -= this._config.energy.move *
      (Math.abs(creature.output[0]) + Math.abs(creature.output[1])) *
      (creature.size / this._config.maxCreatureSize) *
      (type == TileTypes.Land ? 1 : 2); // Three times more energy consuming to swim
    const newX = creature.pos.x + creature.output[0] * creature.maxSpeed /
      (type == TileTypes.Land ? 1 : 1.5); // Twice slow on water
    const newY = creature.pos.y + creature.output[1] * creature.maxSpeed /
      (type == TileTypes.Land ? 1 : 1.5); // Twice slow on water

    creature.pos.x = Math.min(this._config.fieldWidth - 1, Math.max(0, newX));
    creature.pos.y = Math.min(this._config.fieldHeight - 1, Math.max(0, newY));
  }

  metabolize(creature: BaseCreature) {
    creature.size -= this._config.energy.metabolism *
      (creature.age / this._config.reproduceAge) *
      (creature.size / this._config.maxCreatureSize);
  }

  mutate(creature: BaseCreature) {

    const totalProbability = (Math.log(1 - this._config.mutability / 100) / Math.log(0.99)) / this._config.mutability;
    for (let i = 0; i < creature.network.axons.length; i++) {
      for (let j = 0; j < creature.network.axons[i].length; j++) {
        for (let k = 0; k < creature.network.axons[i][j].length; k++) {
          for (let l = 0; l < this._config.mutability; l++) {
            let weight = creature.network.axons[i][j][k];
            let randomNumber = this._noise.noiseF(100);
            const numMutations = 5;

            if (randomNumber < totalProbability * 1 / numMutations) {
              weight *= this._noise.noise();
            } else if (randomNumber < totalProbability * 2 / numMutations) {
              weight /= this._noise.noise() + 1e-10;
            } else if (randomNumber < totalProbability * 3 / numMutations) {
              weight *= -1;
            } else if (randomNumber < totalProbability * 4 / numMutations) {
              weight -= this._noise.noiseF(0.25);
            } else if (randomNumber < totalProbability * 5 / numMutations) {
              weight += this._noise.noiseF(0.25);
            }

            creature.network.axons[i][j][k] = weight;
          }
        }
      }
    }
  }

  format(input: string): number {
    return parseInt(input) + Math.floor(this._noise.noiseF(this._config.maxColorChange * 2) - this._config.maxColorChange);
  }

  mutateColor(color: string): string {
    let tempcolor = color.replace(" ", "").replace("hsl", "").replace("(", "").replace(")", "").split(",");
    let rand = this._noise.noiseF(100);

    if (rand < 10) {
      tempcolor[0] = `${Math.min(356, Math.max(0, this.format(tempcolor[0])))}`;
    } else if (rand < 20) {
      tempcolor[1] = `${Math.min(100, Math.max(0, this.format(tempcolor[1])))}%`;
    } else if (rand < 30) {
      tempcolor[2] = `${Math.min(100, Math.max(0, this.format(tempcolor[2])))}%`;
    }

    return `hsl(${tempcolor.join()})`;
  }

  reproduce(creature: BaseCreature): BaseCreature[] {
    const children: BaseCreature[] = [];
    if (creature.age > this._config.reproduceAge && creature.reproduceTime > this._config.minReproduceTime) {
      if (creature.size > this._config.energy.birth * creature.size + this._config.minCreatureSize) {
        for (let i = 0; i < this._config.numChildren; i++) {
          const child: BaseCreature = this.newCreature();
          this.copyNeuralNetwork(creature, child);
          child.color = this.mutateColor(creature.color);
          child.generation = creature.generation + 1
          this.mutate(child);
          children.push(child);
        }

        creature.size -= this._config.energy.birth * creature.size;
        creature.reproduceTime = 0;
      }
    }

    return children;
  }

  tick(creature: BaseCreature) {
    creature.age++;
    creature.reproduceTime++;
  }

  eat(creature: BaseCreature, tile: Tile) {
    creature.size -= this._config.energy.eat;

    creature.maxSpeed = this._config.maxCreatureSpeed * creature.output[2];

    if (tile.Food - creature.output[2] * this._config.eatSpeed < 0) {
      creature.size += tile.Food / this._config.eatSpeed;
      tile.Food = 0;
    } else if (tile.Food > 0) {
      creature.size += creature.output[2] * this._config.eatSize * (tile.Food / this._config.maxTileFood);
      tile.Food -= creature.output[2] * this._config.eatSpeed;
    }
  }

  update(creatures: BaseCreature[], map: Tile[][]) {
    for (const creature of creatures) {
      creature.maxSpeed = this._config.maxCreatureSpeed;
      const x = creature.pos.x / this._config.fieldWidth;
      const y = creature.pos.y / this._config.fieldHeight;
      const size = (creature.size - this._config.minCreatureSize) / (this._config.maxCreatureSize - this._config.minCreatureSize);
      const tile: Tile = this.getTile(creature, map);
      const tileFood = tile.Food / this._config.maxTileFood;
      const eatPower = creature.output[2];
      const age = creature.age / this._config.reproduceAge;
      const reproduceTime = creature.reproduceTime / this._config.minReproduceTime;
      const xMove = creature.output[0];
      const yMove = creature.output[1];
      const reproduce = creature.output[3];

      const input = [x, y, size, tileFood, eatPower, age, reproduceTime, xMove, yMove, reproduce || 0];
      creature.output = this.feedForward(creature, input);
      this.move(creature, tile.Type);
      this.metabolize(creature);

      if (creature.output[2] > 0) this.eat(creature, tile);
      if (creature.output[3] > 0) {
        let children = this.reproduce(creature);
        children.forEach(child => creatures.push(child));
      }

      this.tick(creature);


      creature.size = Math.min(this._config.maxCreatureSize, creature.size);
      if (creature.size < this._config.minCreatureSize) {
        let pos = creatures.indexOf(creature);
        creatures.splice(pos, 1);
      }
    }

    while (creatures.length < this._config.minCreatures) {
      creatures.push(this.newCreature());
    }

    for (const row of map) {
      for (const tile of row) {
        if (tile.Type === TileTypes.Land) {
          if (tile.Food < this._config.maxTileFood) {
            tile.Food += this._config.foodRegrowRate;
          }
        }
      }
    }
  }

}
