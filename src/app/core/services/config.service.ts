import { Injectable } from '@angular/core';

@Injectable()
export class ConfigService {

  constructor() { }

  public layers: number[] = [10, 6, 4];
  public maxCreatureSize = 35; // Maximum Creature Size
  public minCreatureSize = 10; // Minimum Creature Size

  public offset = 0; // Amount to offset the value of a neuron
  public fieldWidth = 4000;
  public fieldHeight = 4000;
  public zoomLevel = 1 / 3.7;
  public oldest = 0;
  public nnui = {
    xoffset: 1920 - 300,
    yoffset: 100,
    xspacing: 50,
    yspacing: 20,
    size: 30,
    stroke: true,
    maxLineSize: 10,
    minLineSize: 5
  };

  public zoomSpeed = 1;
  public maxZoomLevel = 1;
  public minZoomLevel = 1 / 6;
  public maxColorChange = 10;
  public selectSizeAddition = 20;
  public numChildren = 2;
  public maxCreatureSpeed = 5;
  public mapSize = 40;
  public minCreatures = 10;
  public tileSize = 100;
  public eatSpeed = 5;
  public eatSize = 0.08;
  public energy = {
    eat: 0.01,
    metabolism: 0.01,
    move: 0.01,
    birth: 0.5
  };

  public foodRegrowRate = 0.4;
  public maxTileFood = 1000;
  public reproduceAge = 2000;
  public minReproduceTime = 1000;
  public mutability = 10;
}
