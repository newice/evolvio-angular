import { TestBed, inject } from '@angular/core/testing';

import { SeededNoiseService } from './seeded-noise.service';

describe('SeededNoiseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SeededNoiseService]
    });
  });

  it('should be created', inject([SeededNoiseService], (service: SeededNoiseService) => {
    expect(service).toBeTruthy();
  }));
});
