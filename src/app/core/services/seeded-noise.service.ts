import { Injectable } from '@angular/core';

@Injectable()
export class SeededNoiseService {

  seed: number;
  grv = 0;
  constructor() {
    this.seed = this.newSeed();
  }

  noiseF(factor: number): number {
    return this.noise() * factor;
  }

  noise(): number {
    this.grv++;
    return (Math.abs(
      Math.sin(this.seed) +
      this.seed * this.grv * Math.tan(this.grv) * Math.cos(this.grv) / Math.cos(this.seed / 5) +
      Math.tan(this.seed))) % 1;
  }

  newSeed() {
    return Math.floor(Math.random() * 1000000);
  }
}
