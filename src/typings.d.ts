/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

declare interface CanvasRenderingContext2D {
  fillCircle(x: number, y: number, r: number, s: boolean): void;
}